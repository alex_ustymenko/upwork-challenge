def init_data
  [
      {
          customer: { first_name: 'Johny', last_name: 'Flow' },
          transactions: {
              successful: 5,
              disputed: 3
          }
      },
      {
          customer: { first_name: 'Raj', last_name: 'Jamnis' },
          transactions: {
              successful: 3,
              disputed: 2
          }
      },
      {
          customer: { first_name: 'Andrew', last_name: 'Chung' },
          transactions: {
              failed: 3,
              successful: 1
          }
      },
      {
          customer: { first_name: 'Mike', last_name: 'Smith' },
          transactions: {
              failed: 2,
              successful: 1
          }
      },
  ]
end

Creator.new(init_data).perform