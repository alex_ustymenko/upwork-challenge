class CreateCreditCardCharge < ActiveRecord::Migration
  def change
    create_table :credit_card_charges do |t|
      t.boolean :paid
      t.integer :amount, default: 0
      t.string :currency
      t.boolean :refunded
      t.boolean :disputed

      t.timestamps
    end

    add_reference :credit_card_charges, :customer, index: true
    add_foreign_key :credit_card_charges, :customers
  end
end
