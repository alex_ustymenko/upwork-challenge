# class responsible for creating customers and
# credit_card_charges from JSON input
class Creator
  attr_reader :data

  def initialize(data)
    @data = data
  end

  def perform
    suffix = '_credit_card_charge'
    data.each do |info|
      customer = Customer.new(info[:customer])
      info[:transactions].each do |key, value|
        value.times do
          customer.credit_card_charges.build(CreditCardCharge.send("#{key}#{suffix}"))
          customer.save
        end
      end
    end
  end
end
