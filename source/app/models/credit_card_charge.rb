class CreditCardCharge < ActiveRecord::Base
  belongs_to :customer

  scope :failed, -> { where(failed_credit_card_charge) }
  scope :disputed, -> { where(disputed_credit_card_charge) }
  scope :successful, -> { where(successful_credit_card_charge) }

  def self.successful_credit_card_charge
    { disputed: false }
  end

  def self.failed_credit_card_charge
    { paid: false }
  end

  def self.disputed_credit_card_charge
    { disputed: true }
  end
end