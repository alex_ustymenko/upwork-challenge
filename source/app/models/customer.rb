class Customer < ActiveRecord::Base
  has_many :credit_card_charges, dependent: :delete_all

  def full_name
    [first_name, last_name].join(' ')
  end
end