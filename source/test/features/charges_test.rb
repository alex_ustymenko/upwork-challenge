require "test_helper"

feature "Can Access Charges" do
  include HelperMethods

  before :all do
    Creator.new(init_data).perform

    visit charges_path
  end

  # 1
  scenario "three lists on the screen" do
    page.all(:xpath, '//table/th/h1').map(&:text).size.must_equal 3
  end

  scenario "successful charges list" do
    assert page.has_xpath?("//table/th/h1[text()='Successful Charges']")
  end

  scenario "failed charges list" do
    assert page.has_xpath?("//table/th/h1[text()='Failed Charges']")
  end

  scenario "disputed charges list" do
    assert page.has_xpath?("//table/th/h1[text()='Disputed Charges']")
  end

  # 2
  scenario "successful charges list has 10 rows" do
    page.all(:xpath, "//table[@id='successfull_charges']/tr").size.must_equal 10
  end

  scenario "failed charges list has 5 rows" do
    page.all(:xpath, "//table[@id='failed_charges']/tr").size.must_equal 5
  end

  scenario "disputed charges list has 5 rows" do
    page.all(:xpath, "//table[@id='disputed_charges']/tr").size.must_equal 5
  end
end

def init_data
  [
      {
          customer: { first_name: 'Johny', last_name: 'Flow' },
          transactions: {
              successful: 5,
              disputed: 3
          }
      },
      {
          customer: { first_name: 'Raj', last_name: 'Jamnis' },
          transactions: {
              successful: 3,
              disputed: 2
          }
      },
      {
          customer: { first_name: 'Andrew', last_name: 'Chung' },
          transactions: {
              failed: 3,
              successful: 1
          }
      },
      {
          customer: { first_name: 'Mike', last_name: 'Smith' },
          transactions: {
              failed: 2,
              successful: 1
          }
      },
  ]
end

