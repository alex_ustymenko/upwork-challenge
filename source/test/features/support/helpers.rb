module HelperMethods
  def create_customer_1
    customer_1 = Customer.new(first_name: 'Johny', last_name: 'Flow')

    # successful
    5.times {
      customer_1.credit_card_charges.build(CreditCardCharge.successful_credit_card_charge)
      customer_1.save
    }

    # disputed
    3.times {
      customer_1.credit_card_charges.build(CreditCardCharge.disputed_credit_card_charge)
      customer_1.save
    }
  end

  def create_customer_2
    customer_2 = Customer.new(first_name: 'Raj', last_name: 'Jamnis')

    # successful
    3.times {
      customer_2.credit_card_charges.build(CreditCardCharge.successful_credit_card_charge)
      customer_2.save
    }

    # disputed
    2.times {
      customer_2.credit_card_charges.build(CreditCardCharge.disputed_credit_card_charge)
      customer_2.save
    }
  end

  def create_customer_3
    customer_3 = Customer.new(first_name: 'Andrew', last_name: 'Chung')

    # successful
    customer_3.credit_card_charges.build(CreditCardCharge.successful_credit_card_charge)
    customer_3.save

    # failed
    3.times {
      customer_3.credit_card_charges.build(CreditCardCharge.failed_credit_card_charge)
      customer_3.save
    }
  end

  def create_customer_4
    customer_4 = Customer.new(first_name: 'Mike', last_name: 'Smith')

    # successful
    customer_4.credit_card_charges.build(CreditCardCharge.successful_credit_card_charge)
    customer_4.save

    # failed
    2.times {
      customer_4.credit_card_charges.build(CreditCardCharge.failed_credit_card_charge)
      customer_4.save
    }
  end
end